/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ExceptionUFPS;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author MADARME
 */
public class TestSort {
    
    public static void main(String[] args) throws ExceptionUFPS, Exception {
        ListaS<Integer> l=new ListaS();
        l.insertarAlFinal(3);
        l.insertarAlFinal(2);
        l.insertarAlFinal(9);
        l.insertarAlFinal(7);
        l.insertarAlFinal(5);
        l.insertarAlFinal(1);
        l.insertarAlFinal(8);
        l.insertarAlFinal(4);   
        
        
                
        System.out.println("Lista Desordenada:"+l.toString()); 
        //l.eliminarMayor();
        //System.out.println("Tiene elementos repetidos " + l.contieneElemRepetidos());
        l.ordenamientoInsercionNodos();
        //l.ordenamientoPorInsercionInfos();
        System.out.println("Lista Ordenada:"+l.toString());
        
        /*ListaS<String> l=new ListaS();
        l.insertarAlFinal("GABRIEL");
        l.insertarAlFinal("JUAN");
        l.insertarAlFinal("ANA");
        l.insertarAlFinal("FARID");
        
        System.out.println("El menor de la lista es:"+l.getMenor());
        
        
        ListaS<Persona> personas=new ListaS();
        personas.insertarAlFinal(new Persona("Marco",1000));
       
        personas.insertarAlFinal(new Persona("Mariana",100));
        personas.insertarAlFinal(new Persona("Pepe",101));
         personas.insertarAlFinal(new Persona("Maria",10));
        
        System.out.println("El menor de la lista es:"+personas.getMenor());
        
        
        System.out.println("Lista Desordenada:"+personas.toString());
        //personas.sortSelection();
        personas.ordenamientoPorNodos();
        System.out.println("Lista Ordenada:"+personas.toString());*/
    }
    
}
